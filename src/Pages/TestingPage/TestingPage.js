import React, { useEffect, useState } from "react";
import Header from "../../Components/Header/Header";

export default function TestingPage() {
  //   const [count, setCount] = useState(0);
  //   useEffect(() => {
  //     document.title = `You have clicked ${count} times`;
  //     console.log("render inside useEffect");
  //     return () => {
  //       console.log("this acts as componentWillUnmount");
  //     }; //Phần return của useEffect dùng tương tự như componentWillUnmount()
  //   }, []); //khi state nằm trong array này được re-rendered(hay còn gọi là được updated) thì hàm useEffect mới được chạy lại. Còn nếu bỏ trống array thì useEffect chỉ chạy một lần khi component render lần đầu. Việc này giúp hạn chế những render không cần thiết => tăng tốc cho app
  //   console.log("render outside useEffect");
  //   return (
  //     <div className="flex justify-center items-center h-screen flex-col">
  //       <p>You have clicked {count} times</p>
  //       <button
  //         className="px-3 py-1 bg-yellow-300 mt-5 rounded"
  //         onClick={() => {
  //           setCount(count + 1);
  //         }}
  //       >
  //         plus 1
  //       </button>
  //     </div>
  //   );
  // }

  // import React, { Component } from "react";
  // export default class TestingPage extends Component {
  //   constructor(props) {
  //     super(props);
  //     this.state = {
  //       count: 0,
  //     };
  //   }

  //   componentDidMount() {
  //     document.title = `didMount - You clicked ${this.state.count} times`;
  //     console.log("Didmount");
  //   }
  //   componentDidUpdate() {
  //     document.title = `didUpdate - You've clicked ${this.state.count} times`;
  //     console.log("Didupdate");
  //   }

  //   render() {
  //     return (
  //       <div>
  //         <p>You clicked {this.state.count} times</p>
  //         <button onClick={() => this.setState({ count: this.state.count + 1 })}>
  //           Click me
  //         </button>
  //       </div>
  //     );
  //   }
  // }
  return <Header />;
}
