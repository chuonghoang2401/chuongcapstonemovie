import React from "react";
import { NavLink } from "react-router-dom";
import "./NotFoundPage.css";

export default function NotFoundPage() {
  return (
    <div id="main">
      <div class="fof">
        <h1>Error 404: Page not found!</h1>
        <div className="mt-5">
          <NavLink to="/">
            <button className="px-5 py-3 bg-transparent border-red-600 border rounded text-black shadow">
              Return to <span className="font-semibold">HOMEPAGE</span> now!
            </button>
          </NavLink>
        </div>
      </div>
    </div>
  );
}
