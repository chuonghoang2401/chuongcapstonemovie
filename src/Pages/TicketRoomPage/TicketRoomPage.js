import React from "react";
import { useSelector } from "react-redux";
import TicketSelecting from "./TicketSelecting/TicketSelecting";

export default function TicketRoomPage() {
  let user = useSelector((state) => {
    return state.userSlice.userInfor;
  });
  if (user) {
    return (
      <div>
        <TicketSelecting />
      </div>
    );
  } else {
    window.location.href = "/login";
  }
}
