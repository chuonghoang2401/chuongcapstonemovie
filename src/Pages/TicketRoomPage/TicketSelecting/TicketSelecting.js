import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { TicketRoomService } from "../../../services/TicketRoomService/TicketRoomService";

export default function TicketSelecting() {
  let params = useParams();
  let lichChieuId = params.id;
  useEffect(() => {
    TicketRoomService.getDanhSachPhongVe(lichChieuId)
      .then((res) => {
        console.log(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });

    return () => {};
  }, []);

  return <div>lichChieuid: {lichChieuId}</div>;
}
