import React from "react";
import moment from "moment";
import { NavLink } from "react-router-dom";
export default function MovieTabItem({ movie }) {
  return (
    <div className="flex p-3">
      <img
        className="w-24 h-40 object-cover mr-5 rounded"
        src={movie.hinhAnh}
        alt=""
      />
      <div>
        {/* moment js */}
        <h5 className="font-medium mb-5">{movie.tenPhim}</h5>
        <div className="lg:grid lg:grid-cols-3 lg:gap-4 sm:flex sm:flex-col sm:gap-5">
          {movie.lstLichChieuTheoPhim.slice(0, 9).map((item) => {
            return (
              <NavLink
                to={`/ticketroom/${item.maLichChieu}`}
                className="border-2 border-green-600 rounded p-2 hover:bg-green-500 hover:text-white"
              >
                {moment(item.ngayChieuGioChieu).format("DD/MM/YYYY - hh:mm A")}
              </NavLink>
            );
          })}
        </div>
      </div>
    </div>
  );
}
