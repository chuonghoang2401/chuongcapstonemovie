import { PlusOutlined } from '@ant-design/icons';
import {
  Button,
  Checkbox,
  DatePicker,
  Form,
  Input,
  Radio,
  Upload,
} from 'antd';

import { useState } from 'react';
const { RangePicker } = DatePicker;
const { TextArea } = Input;
const FilmAddPage = () => {
  return (
    <>
      Thêm mới phim
      <Form
        labelCol={{
          span: 4,
        }}
        wrapperCol={{
          span: 14,
        }}
        layout="horizontal"
      >
        <Form.Item label="Form Size">
          <Radio.Group>
            <Radio.Button value="optional">Small</Radio.Button>
            <Radio.Button value>Default</Radio.Button>
            <Radio.Button value={false}>Large</Radio.Button>
          </Radio.Group>
        </Form.Item>
        <Form.Item label="Tên Phim">
          <Input />
        </Form.Item>
        <Form.Item label="Trailer">
          <Input />
        </Form.Item>
        <Form.Item label="Mô Tả">
          <Input />
        </Form.Item>
        <Form.Item label="DatePicker">
          <DatePicker />
        </Form.Item>

        <Form.Item label="Đang chiếu" name="disabled" valuePropName="checked">
          <Checkbox>Checkbox</Checkbox>
        </Form.Item>
        
        <Form.Item label="Sắp chiếu" name="disabled" valuePropName="checked">
          <Checkbox>Checkbox</Checkbox>
        </Form.Item>
       
       
        <Form.Item label="Upload" valuePropName="fileList">
          <Upload action="/upload.do" listType="picture-card">
            <div>
              <PlusOutlined />
              <div
                style={{
                  marginTop: 8,
                }}
              >
                Upload
              </div>
            </div>
          </Upload>
        </Form.Item>
        <Form.Item>
          <Button>Xác Nhận</Button>
        </Form.Item>
      </Form>
    </>
  );
};

export default () => <FilmAddPage />;