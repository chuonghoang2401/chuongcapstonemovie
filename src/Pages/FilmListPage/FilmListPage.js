import React, { useEffect, useState } from "react";
import axios from "axios"
import "./FilmListPage.css";
import { Input, Button } from "antd";

export default function FilmListPage() {
  let [query, setQuery] = useState("");
  const [users, setUser] = useState({});

  const handleOnInputChange = (event) => {
    query = event.target.value
    fetchSearchResults(query);
  };

  const fetchSearchResults = (query) => {
    const TOKEN_CYBERSOFT =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzNSIsIkhldEhhblN0cmluZyI6IjAzLzA2LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY4NTc1MDQwMDAwMCIsIm5iZiI6MTY1NzczMTYwMCwiZXhwIjoxNjg1ODk4MDAwfQ.KXn1XtehbphvfW3OSUFlLIzSrEtSLDtDQG4BgF38Cus";
  
    const searchUrl = `https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim`;
    axios.get(searchUrl, {
      headers: { 'TokenCybersoft': `${TOKEN_CYBERSOFT}`}
    }).then((res) => {
      console.log('chuongchuong', res.data.content);
      setUser(res.data.content)
    });
  };

  const renderSearchResults = () => {
    if (users.length) {
      return (
        <table>
            <tbody>
              <tr>
                <td>
                  <strong>Mã Phim</strong>
                </td>
                <td>
                  <strong>Tên Phim</strong>
                </td>
                <td>
                  <strong>Bí Danh</strong>
                </td>
                <td>
                  <strong>Trailer</strong>
                </td>
                <td>
                  <strong>Hình Ảnh</strong>
                </td>
              </tr>
              {users.map((user, i) => (
                <tr key={i}>
                  <td>{user.maPhim}</td>
                  <td>{user.tenPhim}</td>
                  <td>{user.biDanh}</td>
                  <td>{user.trailer}</td>
                  <td>{user.hinhAnh}</td>
                 
                </tr>
              ))}
            </tbody>
          </table>
      );
    }
  };

  useEffect(() => {
    fetchSearchResults(null)
    return () => {};
  }, []);

  return (
    <div>
      <Button onClick>Thêm phim mới</Button>
      <Input placeholder="input search text" onSearch={handleOnInputChange} style={{ width: 500 }} />
      {renderSearchResults()}
    </div>
  );
}
