import { UserOutlined } from "@ant-design/icons";
import { Layout, Menu, theme } from "antd";
import { useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";

const { Header, Content, Footer, Sider } = Layout;

const items = [
  {
    label: (
      <a href="/admin/users">
        Users
      </a>
    ),
    icon: <UserOutlined />,
    children: [
      {
        label: <a href="/admin/users">Users</a>,
      },
      {
        label: <a href="/admin/users/addnew">Add new</a>,
      },
    ],
  },
  {
    label: (
      <a href="/admin/films">
        Films
      </a>
    ),
    icon: <UserOutlined />,
    children: [
      {
        label: <a href="/admin/films">Films</a>,
      },
      {
        label: <a href="/admin/films/addnew">Add new</a>,
      },
    ],
  },
];

export default function LayoutAdmin({ children }) {
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  return (
    <Layout
      style={{
        minHeight: "100vh",
      }}
    >
      <Sider
        collapsible
        collapsed={collapsed}
        onCollapse={(value) => setCollapsed(value)}
      >
        <div
          style={{
            height: 32,
            margin: 16,
            background: "rgba(255, 255, 255, 0.2)",
          }}
        />
        <Menu
          theme="dark"
          defaultSelectedKeys={["1"]}
          mode="inline"
          items={items}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          style={{
            padding: 0,
            background: colorBgContainer,
          }}
        />
        <Content>
          <div>{children}</div>
        </Content>
        <Footer
          style={{
            textAlign: "center",
          }}
        ></Footer>
      </Layout>
    </Layout>
  );
}
