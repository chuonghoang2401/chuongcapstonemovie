import { createSlice } from "@reduxjs/toolkit";
import { LocalstorageService } from "../../services/LocalstorageService/LocalstorageService";
const initialState = {
  userInfor: LocalstorageService.get(),
};
export const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setUserInfor: (state, action) => {
      state.userInfor = action.payload;
    },
  },
});
export const { setUserInfor } = userSlice.actions;
export default userSlice.reducer;
