import axios from "axios";
import { createConfig } from "../ConfigURL/ConfigURL";
import { BASE_URL } from "../constant/constant";
export const UserService = {
  postDangNhap: (dataUser) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
      method: "POST",
      data: dataUser,
      headers: createConfig(),
    });
  },
  postDangKy: (dataUser) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangKy`,
      method: "POST",
      data: dataUser,
      headers: createConfig(),
    });
  },
};
