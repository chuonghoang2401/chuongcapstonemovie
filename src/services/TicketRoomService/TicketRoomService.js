import axios from "axios";
import { createConfig } from "../ConfigURL/ConfigURL";
import { BASE_URL } from "../constant/constant";

export const TicketRoomService = {
  getDanhSachPhongVe: (maLichChieu) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${maLichChieu}`,
      method: "GET",
      headers: createConfig(),
    });
  },
};
