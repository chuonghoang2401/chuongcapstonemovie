import React from "react";
import Owl from "../OwlCarousel/Owl";

export default function Banner() {
  const renderBanner = () => {
    return <Owl />;
  };
  return <div>{renderBanner()}</div>;
}
