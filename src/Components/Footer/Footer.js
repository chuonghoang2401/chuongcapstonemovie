import React from "react";

export default function Footer() {
  const renderFooter = () => {
    return (
      <div className="sm:px-14 md:px-24 lg:px-40 py-10 bg-neutral-900 text-neutral-400 ">
        <div className="lg:grid grid-cols-6 gap-2 py-5 sm:hidden ">
          <div className="the-film-zone col-span-2">
            <p className="font-semibold text-white pb-5">THE FILM ZONE</p>
            <div className="grid grid-cols-2 ">
              <div>
                <p className="hover:text-white transition">
                  <a href="#">FAQ</a>
                </p>
                <p className="hover:text-white transition">
                  <a href="#">Brand Guidelines</a>
                </p>
              </div>
              <div>
                <p className="hover:text-white transition">
                  <a href="#">Thỏa thuận sử dụng</a>
                </p>
                <p className="hover:text-white transition">
                  <a href="#">Chính sách bảo mật</a>
                </p>
              </div>
            </div>
          </div>
          <div className="doi-tac col-span-2">
            <p className="font-semibold text-white pb-5">ĐỐI TÁC</p>
            <div className="grid grid-cols-4 w-56 h-32 gap-6">
              <img src="/assets/images/brands_logo/cgv.png" alt="" />
              <img src="/assets/images/brands_logo/beta.jpg" alt="" />
              <img src="/assets/images/brands_logo/bhd.png" alt="" />
              <img src="/assets/images/brands_logo/cinestar.png" alt="" />
              <img src="/assets/images/brands_logo/galaxycinema.png" alt="" />
              <img src="/assets/images/brands_logo/megags.png" alt="" />
              <img src="/assets/images/brands_logo/ddc.png" alt="" />
              <img src="/assets/images/brands_logo/galaxycinema.png" alt="" />
            </div>
          </div>
          <div className="mobile-app">
            <p className="font-semibold text-white pb-5">MOBILE APP</p>
            <a href="#" className="mr-5">
              <i class="fab fa-apple text-3xl"></i>
            </a>
            <a href="#">
              <i class="fab fa-android text-3xl"></i>
            </a>
          </div>
          <div className="social">
            <p className="font-semibold text-white pb-5">SOCIALS</p>
            <div className="flex justify-start items-center">
              <a href="#" className="mr-5">
                <i class="fa-brands fa-facebook text-3xl"></i>
              </a>
              <a href="#">
                <img className="h-10 w-10" src="/assets/images/ZaloLogo.svg" />
              </a>
            </div>
          </div>
        </div>
        <div className="grid grid-cols-6 gap-2 place-items-center">
          <img
            className="h-20 w-20 object-contain"
            src="/assets/images/movie-logo-red.png"
            alt=""
          />
          <div className="col-span-4 text-white">
            <p className="pb-5">
              THE FILM ZONE – SẢN PHẨM CỦA CÔNG TY CỔ PHẦN ZION
            </p>
            <p>
              Địa chỉ: Z06 Đường số 13, Phường Tân Thuận Đông, Quận 7, Tp. Hồ
              Chí Minh, Việt Nam. Giấy chứng nhận đăng ký kinh doanh số:
              0101659783, đăng ký thay đổi lần thứ 30, ngày 22 tháng 01 năm 2020
              do Sở kế hoạch và đầu tư Thành phố Hồ Chí Minh cấp. Số Điện Thoại
              (Hotline): 1900 545 436
            </p>
          </div>
          <img
            src="/assets/images/daThongBao-logo.cb85045e.png"
            className="h-40 w-40 object-contain"
            alt=""
          />
        </div>
      </div>
    );
  };
  return <div>{renderFooter()}</div>;
}
