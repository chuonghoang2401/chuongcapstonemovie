import React from "react";

export default function Hamburger() {
  return (
    <div>
      <i class="fa fa-bars text-2xl"></i>
    </div>
  );
}
