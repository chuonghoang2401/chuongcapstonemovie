import React from "react";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import "./owl.css";

export default function Owl() {
  let banner1 =
    "assets/images/banners/banner-ban-tay-diet-quy-evil-expeller.png";
  let banner2 = "assets/images/banners/banner-lat-mat.png";
  let banner3 = "assets/images/banners/banner-nguoi-nhan-ban.png";
  return (
    <div className="h-5/6">
      <OwlCarousel
        items={1}
        className="owl-theme"
        loop
        autoplay
        autoplayTimeout={3000}
        margin={8}
      >
        <img className="img" src={banner1} />
        <img className="img" src={banner2} />
        <img className="img" src={banner3} />
      </OwlCarousel>
    </div>
  );
}
