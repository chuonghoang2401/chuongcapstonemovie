import React from "react";
import HomePage from "./Pages/HomePage/HomePage";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import LoginPage from "./Pages/LoginPage/LoginPage";
import RegisterPage from "./Pages/RegisterPage/RegisterPage";
import Layout from "./HOC/Layout/Layout";
import LayoutAdmin from "./HOC/LayoutAdmin/LayoutAdmin";
import MovieDetailPage from "./Pages/MovieDetailPage/MovieDetailPage";
import NotFoundPage from "./Pages/NotFoundPage/NotFoundPage";
import TestingPage from "./Pages/TestingPage/TestingPage";
import FilmListPage from "./Pages/FilmListPage/FilmListPage";
import FilmAddPage from "./Pages/FilmAddPage/FilmAddPage";
import UserListPage from "./Pages/UserListPage/UserListPage";
import UserAddPage from "./Pages/UserAddPage/UserAddPage";
import TicketRoomPage from './Pages/TicketRoomPage/TicketRoomPage';
import TicketSelecting from './Pages/TicketRoomPage/TicketSelecting/TicketSelecting';

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="*" element={<NotFoundPage />} />
          <Route
            path="/"
            element={
              <Layout>
                <HomePage />
              </Layout>
            }
          />
          <Route
            path="/login"
            element={
              <Layout>
                <LoginPage />
              </Layout>
            }
          />
          <Route
            path="/register"
            element={
              <Layout>
                <RegisterPage />
              </Layout>
            }
          />
          <Route
            path="/detail/:id"
            element={
              <Layout>
                <MovieDetailPage />
              </Layout>
            }
          />
          <Route
            path="/admin/films"
            element={
              <LayoutAdmin>
                <FilmListPage />
              </LayoutAdmin>
            }
          />
          <Route
            path="/admin/films/addnew"
            element={
              <LayoutAdmin>
                <FilmAddPage />
              </LayoutAdmin>
            }
          />
          <Route
            path="/admin/users/"
            element={
              <LayoutAdmin>
                <UserListPage />
              </LayoutAdmin>
            }
          />
          <Route
            path="/admin/users/addnew"
            element={
              <LayoutAdmin>
                <UserAddPage />
              </LayoutAdmin>
            }
          />
          <Route path="testing-component" element={<TestingPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
